/**
 * Created by rcarroll on 11/19/15.
 */
sealed trait Color

case object Red extends Color

case object Black extends Color
