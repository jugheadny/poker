/**
 * Created by rcarroll on 11/19/15.
 */
sealed trait Rank

case object Jack extends Rank

case object Queen extends Rank

case object King extends Rank

case object Ace extends Rank

case class Numeric(value: Int) extends Rank