/**
 * Created by rcarroll on 11/19/15.
 */

case class Card(rank: Rank, suit: Suit) {
}

object Card {
  def ranks = List(Numeric(2), Numeric(3), Numeric(4), Numeric(5), Numeric(6), Numeric(7), Numeric(8), Numeric(9), Numeric(10), Jack, Queen, King, Ace)
  def suites = Set(Spade, Heart, Club, Diamond)
}
