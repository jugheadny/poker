/**
 * Created by rcarroll on 11/19/15.
 */
sealed trait Suit

case object Club extends Suit

case object Diamond extends Suit

case object Heart extends Suit

case object Spade extends Suit

