/**
 * Created by rcarroll on 11/19/15.
 */
sealed trait Value

case object StraightFlush extends Value

case object FourOfKind extends Value

case object FullHouse extends Value

case object Flush extends Value

case object Straight extends Value

case object ThreeOfKind extends Value

case object TwoPair extends Value

case object Pair extends Value

case class HighCard(value: Int) extends Value


object Value {
  type Hand = (Card, Card, Card, Card, Card)

  def evaluate(hand: Hand): Value =
    if (isStraightFlush(hand)) StraightFlush
    else if (isFourOfKind(hand)) FourOfKind
    else if (isFullHouse(hand)) FullHouse
    else if (isFlush(hand)) Flush
    else if (isStraight(hand)) Straight
    else if (isThreeOfKind(hand)) ThreeOfKind
    else if (isTwoPair(hand)) TwoPair
    else if (isPair(hand)) Pair
    else HighCard(toRankValues(hand).sorted.reverse.toList(0))

  private def toRankSizes(hand: Hand): List[Int] =
    toRanks(hand).groupBy(identity).map { case (rank, listofranks) => listofranks.size }.toList
  private def isFourOfKind(hand: Hand): Boolean = toRankSizes(hand).exists(_ >= 4)

  private def isFullHouse(hand: Hand): Boolean = {
    val rankSizes = toRankSizes(hand)
    rankSizes.exists(_ == 2) && rankSizes.exists(_ == 3)
  }

  private def isFlush(hand: Hand): Boolean = isSameSuit(hand)

  private def isThreeOfKind(hand: Hand): Boolean = toRankSizes(hand).exists(_ >= 3)

  private def isTwoPair(hand: Hand): Boolean = toRankSizes(hand).filter(_ >= 2).size == 2

  private def isPair(hand: Hand): Boolean = toRankSizes(hand).exists(_ >= 2)

  private def isStraightFlush(hand: Hand): Boolean = isSameSuit(hand) && isStraight(hand)

  private def isSameSuit(hand: Hand): Boolean = toSuites(hand).forall(_ == hand._1)

  private def toSuites(hand: Hand): List[Suit] = {
    List(hand._1.suit, hand._2.suit, hand._3.suit, hand._4.suit, hand._5.suit)
  }

  private def isStraight(hand: Hand): Boolean = {
    val rankValues = toRankValues(hand)
    val minimumRankValue = rankValues.min
    rankValues.toSet == minimumRankValue.to(minimumRankValue + 4).toSet
  }

  private def toRankValues(hand: Hand): List[Int] = toRanks(hand).map(valueOf _)

  private def toRanks(hand: Hand): List[Rank] = {
    List(hand._1.rank, hand._2.rank, hand._3.rank, hand._4.rank, hand._5.rank)
  }

  private def valueOf(rank: Rank): Int = rank match {
    case Ace => 14
    case King => 13
    case Queen => 12
    case Jack => 11
    case Numeric(n) => n
  }
}