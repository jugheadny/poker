import scala.util.Random

/**
 * Created by rcarroll on 11/19/15.
 */

class Deck(pCards: List[Card] = for (r <- Card.ranks; s <- Card.suites) yield Card(r, s)) {
  private var top = -1

  val cards = if (isValidDeck(pCards)) {
    pCards
  } else {
    throw new RuntimeException("Deck is invalid!")
  }

  private def shuffle() = new Deck(Random.shuffle(cards))

  def deal() = {
    top += 1
    //if (top >= NUM_CARDS) -1 else card(top)
    cards(top);
  }

  private def isValidDeck(cards: List[Card]) = cards.size <= 52 && cards.distinct.size == cards.size
}

object Deck {
  def shuffle() = new Deck().shuffle()
}