/**
 * Created by rcarroll on 11/19/15.
 */
object Main extends App{
  var deck = Deck.shuffle()


  val hand = (deck.deal(), deck.deal(), deck.deal(), deck.deal(), deck.deal())
  println(hand)

  println(deck.cards.length)

  val value = Value.evaluate(hand)
  println(value)
}
